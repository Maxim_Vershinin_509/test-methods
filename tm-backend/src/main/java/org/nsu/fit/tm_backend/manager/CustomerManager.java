package org.nsu.fit.tm_backend.manager;

import org.nsu.fit.tm_backend.utils.CustomerManagerConstraints;
import org.slf4j.Logger;
import org.nsu.fit.tm_backend.database.IDBService;
import org.nsu.fit.tm_backend.database.data.ContactPojo;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.TopUpBalancePojo;
import org.nsu.fit.tm_backend.manager.auth.data.AuthenticatedUserDetails;
import org.nsu.fit.tm_backend.shared.Globals;

import java.util.*;
import java.util.regex.Pattern;

import static org.nsu.fit.tm_backend.utils.CustomerManagerConstraints.*;

public class CustomerManager extends ParentManager {
    private final List<CustomerManagerConstraints.ConstraintChecker<CustomerPojo>> createCustomerConstraintsChecker = new ArrayList<>();

    public CustomerManager(IDBService dbService, Logger flowLog) {
        super(dbService, flowLog);
        initCreateCustomerConstraintsChecker();
    }

    /**
     * Метод создает новый объект класса Customer. Ограничения:
     * Аргумент 'customer' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * balance - должно быть равно 0 перед отправкой базу данных.
     */
    public CustomerPojo createCustomer(CustomerPojo customer) {
        for (ConstraintChecker<CustomerPojo> constraintChecker : createCustomerConstraintsChecker) {
            if (constraintChecker.test(customer)) {
                throw new IllegalArgumentException(constraintChecker.getConstraint().getMessage());
            }
        }

        // Лабораторная 2: добавить код который бы проверял, что нет customer'а c таким же login (email'ом).
        // Попробовать добавить другие ограничения, посмотреть как быстро растет кодовая база тестов.

        return dbService.createCustomer(customer);
    }

    /**
     * Метод возвращает список customer'ов.
     */
    public List<CustomerPojo> getCustomers() {
        return dbService.getCustomers();
    }

    public CustomerPojo getCustomer(UUID customerId) {
        return dbService.getCustomer(customerId);
    }

    public CustomerPojo lookupCustomer(String login) {
        return dbService.getCustomers().stream()
                .filter(x -> x.login.equals(login))
                .findFirst()
                .orElse(null);
    }

    public ContactPojo me(AuthenticatedUserDetails authenticatedUserDetails) {
        ContactPojo contactPojo = new ContactPojo();

        if (authenticatedUserDetails.isAdmin()) {
            contactPojo.login = Globals.ADMIN_LOGIN;

            return contactPojo;
        }

        // Лабораторная 2: обратите внимание что вернули данных больше чем надо...
        // т.е. getCustomerByLogin честно возвратит все что есть в базе данных по этому customer'у.
        // необходимо написать такой unit тест, который бы отлавливал данное поведение.
        contactPojo = dbService.getCustomerByLogin(authenticatedUserDetails.getName());
        contactPojo.pass = "";
        return contactPojo;
    }

    public void deleteCustomer(UUID id) {
        dbService.deleteCustomer(id);
    }

    /**
     * Метод добавляет к текущему баласу переданное значение, которое должно быть строго больше нуля.
     */
    public CustomerPojo topUpBalance(TopUpBalancePojo topUpBalancePojo) {
        ConstraintChecker<TopUpBalancePojo> balanceChecker = new ConstraintChecker<>(TOP_UP_BALANCE_NOT_GREATER_ZERO, b -> b.money <= 0);

        if (balanceChecker.test(topUpBalancePojo)) {
            throw new IllegalArgumentException(balanceChecker.getConstraint().getMessage());
        }

        CustomerPojo customerPojo = dbService.getCustomer(topUpBalancePojo.customerId);

        customerPojo.balance += topUpBalancePojo.money;

        dbService.editCustomer(customerPojo);

        return customerPojo;
    }

    private void initCreateCustomerConstraintsChecker() {
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(CUSTOMER_IS_NULL, Objects::isNull));

        // First name constraints
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(FIRST_NAME_NULL, c -> c.firstName == null));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(FIRST_NAME_CONTAINS_SPACE, c -> c.firstName.contains(" ")));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(FIRST_NAME_BAD_LENGTH, c -> c.firstName.length() < 2 || c.firstName.length() > 12));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(FIRST_NAME_START_WITH_LOWERCASE_LETTER, c -> !Character.isUpperCase(c.firstName.charAt(0))));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(FIRST_NAME_ANOTHER_LETTERS_ARE_UPPERCASE, c -> c.firstName.substring(1).matches(".*[A-Z].*")));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(FIRST_NAME_CONTAINS_NO_LETTERS, c -> c.firstName.matches(".*[^a-zA-Z].*")));

        // Last name constraints
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LAST_NAME_NULL, c -> c.lastName == null));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LAST_NAME_CONTAINS_SPACE, c -> c.lastName.contains(" ")));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LAST_NAME_BAD_LENGTH, c -> c.lastName.length() < 2 || c.lastName.length() > 12));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LAST_NAME_START_WITH_LOWERCASE_LETTER, c -> !Character.isUpperCase(c.lastName.charAt(0))));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LAST_NAME_ANOTHER_LETTERS_ARE_UPPERCASE, c -> c.lastName.substring(1).matches(".*[A-Z].*")));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LAST_NAME_CONTAINS_NO_LETTERS, c -> c.lastName.matches(".*[^a-zA-Z].*")));

        // Email constraints
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LOGIN_IS_NULL, c -> c.login == null));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LOGIN_BAD_PATTERN, c -> !Pattern.compile("^.+@.+\\..+$").matcher(c.login).matches()));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(LOGIN_NOT_UNIQUE, c -> {
            boolean isFound;
            try {
                dbService.getCustomerByLogin(c.login);
                isFound = true;
            } catch (IllegalArgumentException customerNotFoundException) {
                isFound = false;
            }

            return isFound;
        }));

        // Password constraints
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(PASSWORD_FIELD_IS_NULL, c -> c.pass == null));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(PASSWORD_ILLEGAL_LENGTH, c -> c.pass.length() < 6 || c.pass.length() > 12));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(PASSWORD_EASY, c -> "123qwe".equalsIgnoreCase(c.pass) || "1q2w3e".equalsIgnoreCase(c.pass)));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(PASSWORD_CONTAINS_FIRST_NAME, c -> c.pass.contains(c.firstName)));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(PASSWORD_CONTAINS_LAST_NAME, c -> c.pass.contains(c.lastName)));
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(PASSWORD_CONTAINS_LOGIN, c -> c.pass.contains(c.login)));

        // Balance constraints
        createCustomerConstraintsChecker.add(new ConstraintChecker<>(BALANCE_NOT_ZERO, c -> c.balance != 0));
    }
}
