package org.nsu.fit.tm_backend.utils;

import java.util.function.Predicate;

public enum CustomerManagerConstraints {
    CUSTOMER_IS_NULL("Argument 'customer' is null."),

    FIRST_NAME_NULL("First name is null."),
    FIRST_NAME_CONTAINS_SPACE("First name contains space symbols."),
    FIRST_NAME_BAD_LENGTH("First name's length less than 2 or greater than 12."),
    FIRST_NAME_START_WITH_LOWERCASE_LETTER("First name does not start with a capital letter."),
    FIRST_NAME_ANOTHER_LETTERS_ARE_UPPERCASE("First name contains capital letter other than start."),
    FIRST_NAME_CONTAINS_NO_LETTERS("First name contains no letters."),

    LAST_NAME_NULL("Last name is null."),
    LAST_NAME_CONTAINS_SPACE("Last name contains space symbols."),
    LAST_NAME_BAD_LENGTH("Last name's length less than 2 or greater than 12."),
    LAST_NAME_START_WITH_LOWERCASE_LETTER("Last name does not start with a capital letter."),
    LAST_NAME_ANOTHER_LETTERS_ARE_UPPERCASE("Last name contains capital letter other than start."),
    LAST_NAME_CONTAINS_NO_LETTERS("last name contains no letters."),

    LOGIN_IS_NULL("Login is null."),
    LOGIN_BAD_PATTERN("Login bad pattern."),
    LOGIN_NOT_UNIQUE("The customer's login is not unique."),

    PASSWORD_FIELD_IS_NULL("Field 'customer.pass' is null."),
    PASSWORD_ILLEGAL_LENGTH("Password's length should be more or equal 6 symbols and less or equal 12 symbols."),
    PASSWORD_EASY("Password is very easy."),
    PASSWORD_CONTAINS_FIRST_NAME("Password contains first name."),
    PASSWORD_CONTAINS_LAST_NAME("Password contains last name."),
    PASSWORD_CONTAINS_LOGIN("Password contains login."),

    BALANCE_NOT_ZERO("Balance need is zero."),

    TOP_UP_BALANCE_NOT_GREATER_ZERO("Top up balance not greater than zero.");

    public static class ConstraintChecker<T> {
        private final CustomerManagerConstraints constraint;
        private final Predicate<T> test;

        public ConstraintChecker(final CustomerManagerConstraints constraint,
                                 final Predicate<T> test) {
            this.constraint = constraint;
            this.test = test;
        }

        public boolean test(T t) {
            return test.test(t);
        }

        public CustomerManagerConstraints getConstraint() {
            return constraint;
        }
    }

    private final String message;

    CustomerManagerConstraints(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
