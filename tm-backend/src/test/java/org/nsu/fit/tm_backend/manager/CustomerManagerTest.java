package org.nsu.fit.tm_backend.manager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nsu.fit.tm_backend.database.IDBService;
import org.nsu.fit.tm_backend.database.data.ContactPojo;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.TopUpBalancePojo;
import org.nsu.fit.tm_backend.manager.auth.data.AuthenticatedUserDetails;
import org.nsu.fit.tm_backend.shared.Authority;
import org.nsu.fit.tm_backend.shared.Globals;
import org.nsu.fit.tm_backend.utils.CustomerManagerConstraints;
import org.slf4j.Logger;

import java.sql.SQLException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.nsu.fit.tm_backend.utils.CustomerManagerConstraints.*;


// Лабораторная 2: покрыть юнит тестами класс CustomerManager на 100%.
class CustomerManagerTest {
    private Logger logger;
    private IDBService dbService;
    private CustomerManager customerManager;

    private CustomerPojo customerInput;

    private final int sizeOfListOfSomeCustomers = 4;

    private void checkCreateCustomerBadCase(CustomerPojo customerPojo, CustomerManagerConstraints constraint) {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.createCustomer(customerPojo));
        assertEquals(constraint.getMessage(), exception.getMessage());

        verify(dbService, times(0)).createCustomer(customerInput);
    }

    @BeforeEach
    void init() {
        // Создаем mock объекты.
        dbService = mock(IDBService.class);
        logger = mock(Logger.class);

        // Создаем класс, методы которого будем тестировать,
        // и передаем ему наши mock объекты.
        customerManager = new CustomerManager(dbService, logger);

        customerInput = new CustomerPojo();
        customerInput.firstName = "John";
        customerInput.lastName = "Wick";
        customerInput.login = "n@mail.com";
        customerInput.pass = "Baba_Jaga";
        customerInput.balance = 0;
    }

    /*
    *
    *   Tests for 'createCustomer' method
    *
     */

    @Test
    void testCreateCustomer() {
        CustomerPojo createCustomerOutput = new CustomerPojo();
        createCustomerOutput.id = UUID.randomUUID();
        createCustomerOutput.firstName = "John";
        createCustomerOutput.lastName = "Wick";
        createCustomerOutput.login = "john_wick@example.com";
        createCustomerOutput.pass = "Baba_Jaga";
        createCustomerOutput.balance = 0;

        when(dbService.createCustomer(customerInput)).thenReturn(createCustomerOutput);
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.createCustomer(customerInput);

        // Проверяем результат выполенния метода
        assertEquals(customer.id, createCustomerOutput.id);

        // Проверяем, что метод по созданию Customer был вызван ровно 1 раз с определенными аргументами
        verify(dbService, times(1)).createCustomer(customerInput);
    }

    @Test
    void testCreateCustomerWithNullArgument_Right() {
        checkCreateCustomerBadCase(null, CUSTOMER_IS_NULL);
    }

    @Test
    void testCreateCustomerWithNullFirstName() {
        customerInput.firstName = null;
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_NULL);
    }

    @Test
    void testCreateCustomerWithFirstNameContainsSpace() {
        customerInput.firstName = "Name ";
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_CONTAINS_SPACE);
    }

    @Test
    void testCreateCustomerWithFirstNameContainsSpaces() {
        customerInput.firstName = "Name name ";
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_CONTAINS_SPACE);
    }

    @Test
    void testCreateCustomerWithFirstNameShortLength() {
        customerInput.firstName = "N";
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_BAD_LENGTH);
    }

    @Test
    void testCreateCustomerWithFirstNameLongLength() {
        customerInput.firstName = "Namenamenamen";
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_BAD_LENGTH);
    }

    @Test
    void testCreateCustomerWithFirstNameNotCapitalLetter() {
        customerInput.firstName = "name";
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_START_WITH_LOWERCASE_LETTER);
    }

    @Test
    void testCreateCustomerWithFirstNameCapitalLettersAfterFirst() {
        customerInput.firstName = "NAme";
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_ANOTHER_LETTERS_ARE_UPPERCASE);
    }

    @Test
    void testCreateCustomerWithFirstNameContainsNotLetters() {
        customerInput.firstName = "Name&";
        checkCreateCustomerBadCase(customerInput, FIRST_NAME_CONTAINS_NO_LETTERS);
    }

    @Test
    void testCreateCustomerWithNullLastName() {
        customerInput.lastName = null;
        checkCreateCustomerBadCase(customerInput, LAST_NAME_NULL);
    }

    @Test
    void testCreateCustomerWithLastNameContainsSpace() {
        customerInput.lastName = "Name ";
        checkCreateCustomerBadCase(customerInput, LAST_NAME_CONTAINS_SPACE);
    }

    @Test
    void testCreateCustomerWithLastNameContainsSpaces() {
        customerInput.lastName = "Name name ";
        checkCreateCustomerBadCase(customerInput, LAST_NAME_CONTAINS_SPACE);
    }

    @Test
    void testCreateCustomerWithLastNameShortLength() {
        customerInput.lastName = "N";
        checkCreateCustomerBadCase(customerInput, LAST_NAME_BAD_LENGTH);
    }

    @Test
    void testCreateCustomerWithLastNameLongLength() {
        customerInput.lastName = "Namenamenamen";
        checkCreateCustomerBadCase(customerInput, LAST_NAME_BAD_LENGTH);
    }

    @Test
    void testCreateCustomerWithLastNameNotCapitalLetter() {
        customerInput.lastName = "name";
        checkCreateCustomerBadCase(customerInput, LAST_NAME_START_WITH_LOWERCASE_LETTER);
    }

    @Test
    void testCreateCustomerWithLastNameCapitalLettersAfterFirst() {
        customerInput.lastName = "NAme";
        checkCreateCustomerBadCase(customerInput, LAST_NAME_ANOTHER_LETTERS_ARE_UPPERCASE);
    }

    @Test
    void testCreateCustomerWithLastNameContainsNotLetters() {
        customerInput.lastName = "Name&";
        checkCreateCustomerBadCase(customerInput, LAST_NAME_CONTAINS_NO_LETTERS);
    }

    @Test
    void testCreateCustomerWithLoginNull() {
        customerInput.login = null;
        checkCreateCustomerBadCase(customerInput, LOGIN_IS_NULL);
    }

    @Test
    void testCreateCustomerWithLoginBadPattern1() {
        customerInput.login = "example.com";
        checkCreateCustomerBadCase(customerInput, LOGIN_BAD_PATTERN);
    }

    @Test
    void testCreateCustomerWithLoginBadPattern2() {
        customerInput.login = "example@com.";
        checkCreateCustomerBadCase(customerInput, LOGIN_BAD_PATTERN);
    }

    @Test
    void testCreateCustomerWithLoginBadPattern3() {
        customerInput.login = "example";
        checkCreateCustomerBadCase(customerInput, LOGIN_BAD_PATTERN);
    }

    @Test
    void testCreateCustomerWithLoginBadPattern4() {
        customerInput.login = "example@.com";
        checkCreateCustomerBadCase(customerInput, LOGIN_BAD_PATTERN);
    }

    @Test
    void testCreateCustomerWithLoginAlreadyExists() {
        customerInput.login = "example@mail.com";

        CustomerPojo existsCustomerOutput = new CustomerPojo();
        existsCustomerOutput.id = UUID.randomUUID();
        existsCustomerOutput.firstName = "John2";
        existsCustomerOutput.lastName = "Wick2";
        existsCustomerOutput.login = "example@mail.com";
        existsCustomerOutput.pass = "Baba_Jaga2";
        existsCustomerOutput.balance = 100;

        when(dbService.getCustomerByLogin(customerInput.login)).thenReturn(existsCustomerOutput);
        checkCreateCustomerBadCase(customerInput, LOGIN_NOT_UNIQUE);
    }

    @Test
    void testCreateCustomerWithNullPassword() {
        customerInput.pass = null;
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_FIELD_IS_NULL);
    }

    @Test
    void testCreateCustomerWithShortPassword() {
        customerInput.pass = "12345";
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_ILLEGAL_LENGTH);
    }

    @Test
    void testCreateCustomerWithLongPassword() {
        customerInput.pass = "1234567890123";
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_ILLEGAL_LENGTH);
    }

    @Test
    void testCreateCustomerWithEasyPassword1() {
        customerInput.pass = "123qwe";
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_EASY);
    }

    @Test
    void testCreateCustomerWithEasyPassword2() {
        customerInput.pass = "123QWE";
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_EASY);
    }

    @Test
    void testCreateCustomerWithEasyPassword3() {
        customerInput.pass = "1q2w3e";
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_EASY);
    }

    @Test
    void testCreateCustomerWithPasswordContainsFirstName() {
        customerInput.pass = "qwe" + customerInput.firstName;
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_CONTAINS_FIRST_NAME);
    }

    @Test
    void testCreateCustomerWithPasswordContainsLastName() {
        customerInput.pass = "qwe" + customerInput.lastName;
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_CONTAINS_LAST_NAME);
    }

    @Test
    void testCreateCustomerWithPasswordContainsLogin() {
        customerInput.pass = "q" + customerInput.login;
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, PASSWORD_CONTAINS_LOGIN);
    }

    @Test
    void testCreateCustomerWithBalanceLessZero() {
        customerInput.balance = -1;
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, BALANCE_NOT_ZERO);
    }

    @Test
    void testCreateCustomerWithBalanceGreaterZero() {
        customerInput.balance = 1;
        when(dbService.getCustomerByLogin(customerInput.login)).thenThrow(IllegalArgumentException.class);
        checkCreateCustomerBadCase(customerInput, BALANCE_NOT_ZERO);
    }

    /*
     *
     *   Tests for 'getCustomers' method
     *
     */

    private void checkGetCustomersOutputCorrect(List<CustomerPojo> dbContent, int wantedNumberOfInvocations) {
        when(dbService.getCustomers()).thenReturn(dbContent);

        // Вызываем метод, который хотим протестировать
        List<CustomerPojo> customers = customerManager.getCustomers();

        // Проверяем результат выполенния метода
        assertEquals(customers.size(), dbContent.size());
        for(int i = 0; i < customers.size(); ++i) {
            assertEquals(customers.get(i), dbContent.get(i));
        }

        // Проверяем, что метод получения Customer'ов был вызван нужное количество раз
        verify(dbService, times(wantedNumberOfInvocations)).getCustomers();
    }

    private void setCustomerInputWithSpecialPostfix(CustomerPojo customer, int postfix) {
        customer.id = UUID.randomUUID();
        customer.firstName = "John" + postfix;
        customer.lastName = "Wick" + postfix;
        customer.login = "john_wick" + postfix + "@example.com";
        customer.pass = "Baba_Jaga" + postfix;
        customer.balance = postfix + 1;
    }

    @Test
    void testGetCustomers() {
        List<CustomerPojo> getCustomersOutput = new ArrayList<>();
        int wantedNumberOfInvocations = 0;

        // Проверяем корректность работы с пустым списком
        checkGetCustomersOutputCorrect(getCustomersOutput, ++wantedNumberOfInvocations);

        getCustomersOutput.add(customerInput);

        // Проверяем корректность работы со списком из одного элемента
        checkGetCustomersOutputCorrect(getCustomersOutput, ++wantedNumberOfInvocations);

        for(int i = 1; i < sizeOfListOfSomeCustomers; ++i) {
            setCustomerInputWithSpecialPostfix(customerInput, i);

            getCustomersOutput.add(customerInput);

            checkGetCustomersOutputCorrect(getCustomersOutput, ++wantedNumberOfInvocations);
        }
    }

    /*
     *
     *   Tests for 'getCustomer' method
     *
     */

    @Test
    void testGetCustomer() {

        when(dbService.getCustomer(customerInput.id)).thenReturn(customerInput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.getCustomer(customerInput.id);

        // Проверяем результат выполенния метода
        assertEquals(customer, customerInput);

        // Проверяем, что метод получения Customer'а был вызван нужное количество раз
        verify(dbService, times(1)).getCustomer(customerInput.id);
    }

    @Test
    void testGetUncreatedCustomer() {

        Exception e = new IllegalArgumentException("Customer with id '" + customerInput.id + " was not found.");
        when(dbService.getCustomer(customerInput.id)).thenThrow(e);

        // Вызываем метод, который хотим протестировать
        Exception exception = assertThrows(IllegalArgumentException.class, () -> customerManager.getCustomer(customerInput.id));

        // Проверяем результат выполенния метода
        assertEquals(exception.getMessage(), e.getMessage());

        // Проверяем, что метод получения Customer'а был вызван нужное количество раз
        verify(dbService, times(1)).getCustomer(customerInput.id);
    }

    @Test
    void testGetCustomerRuntimeException() {

        Exception e = new RuntimeException("Exception message");
        when(dbService.getCustomer(customerInput.id)).thenThrow(e);

        // Вызываем метод, который хотим протестировать
        Exception exception = assertThrows(RuntimeException.class, () -> customerManager.getCustomer(customerInput.id));

        // Проверяем результат выполенния метода
        assertEquals(exception.getMessage(), e.getMessage());

        // Проверяем, что метод получения Customer'а был вызван нужное количество раз
        verify(dbService, times(1)).getCustomer(any());
    }

    /*
     *
     *   Tests for 'getCustomer' method
     *
     */

    private void checkLookupCustomerOutputCorrect(List<CustomerPojo> getCustomersOutput, String login,CustomerPojo lookupCustomerOutput, int wantedNumberOfInvocations) {
        when(dbService.getCustomers()).thenReturn(getCustomersOutput);

        // Вызываем метод, который хотим протестировать
        CustomerPojo customer = customerManager.lookupCustomer(login);

        // Проверяем результат выполенния метода
        assertEquals(customer, lookupCustomerOutput);

        // Проверяем, что метод получения Customer'ов был вызван нужное количество раз
        verify(dbService, times(wantedNumberOfInvocations)).getCustomers();
    }

    @Test
    void testLookupCustomer() {
        List<CustomerPojo> getCustomersOutput = new ArrayList<>();
        int wantedNumberOfInvocations = 0;

        getCustomersOutput.add(customerInput);

        // Проверяем корректность работы со списком из одного элемента
        checkLookupCustomerOutputCorrect(getCustomersOutput, customerInput.login, customerInput, ++wantedNumberOfInvocations);

        // Проверяем корректность работы со списком из нескольких элементов
        for(int i = 1; i < sizeOfListOfSomeCustomers; ++i) {
            setCustomerInputWithSpecialPostfix(customerInput, i);

            getCustomersOutput.add(customerInput);

            checkLookupCustomerOutputCorrect(getCustomersOutput, customerInput.login, customerInput, ++wantedNumberOfInvocations);
        }

        // Проверяем корректность работы со списком из нескольких элементов, когда нужный логин в середине списка всех пользователей
        customerInput = getCustomersOutput.get(sizeOfListOfSomeCustomers / 2);
        checkLookupCustomerOutputCorrect(getCustomersOutput, customerInput.login, customerInput, ++wantedNumberOfInvocations);
    }

    @Test
    void testLookupCustomerButCustomersDontExist() {
        List<CustomerPojo> getCustomersOutput = new ArrayList<>();
        int wantedNumberOfInvocations = 1;

        // Проверяем корректность работы с пустым списком
        checkLookupCustomerOutputCorrect(getCustomersOutput, "any@example.com", null, wantedNumberOfInvocations);
    }

    @Test
    void testLookupUncreatedCustomer() {
        List<CustomerPojo> getCustomersOutput = new ArrayList<>();
        int wantedNumberOfInvocations = 0;

        getCustomersOutput.add(customerInput);

        // Проверяем корректность работы со списком из одного элемента
        checkLookupCustomerOutputCorrect(getCustomersOutput, "non-existent@example.com", null, ++wantedNumberOfInvocations);

        // Проверяем корректность работы со списком из нескольких элементов
        for(int i = 1; i < sizeOfListOfSomeCustomers; ++i) {
            setCustomerInputWithSpecialPostfix(customerInput, i);

            getCustomersOutput.add(customerInput);

            checkLookupCustomerOutputCorrect(getCustomersOutput, "non-existent@example.com", null, ++wantedNumberOfInvocations);
        }
    }

    @Test
    void testLookupCustomerRuntimeException() {

        Exception e = new RuntimeException("Exception message");
        when(dbService.getCustomers()).thenThrow(e);

        // Вызываем метод, который хотим протестировать
        Exception exception = assertThrows(RuntimeException.class, () -> customerManager.lookupCustomer(customerInput.login));

        // Проверяем результат выполенния метода
        assertEquals(exception.getMessage(), e.getMessage());

        // Проверяем, что метод получения Customer'ов был вызван нужное количество раз
        verify(dbService, times(1)).getCustomers();
    }


    /*
     *
     *   Tests for 'me' method
     *
     */

    private void checkOnlyLoginWasReturned(ContactPojo meMethodResult) {
        assertEquals(0, meMethodResult.balance);
        assertNull(meMethodResult.firstName);
        assertNull(meMethodResult.lastName);
        assertNull(meMethodResult.pass);
    }

    @Test
    void testMeAdmin() {
        Set<String> authorities = new HashSet<>();
        authorities.add(Authority.ADMIN_ROLE);
        AuthenticatedUserDetails userDetails = new AuthenticatedUserDetails("12345", "John", authorities);

        ContactPojo meMethodResult = customerManager.me(userDetails);
        assertEquals(Globals.ADMIN_LOGIN, meMethodResult.login);
        checkOnlyLoginWasReturned(meMethodResult);

        verify(dbService, atMost(1)).getCustomerByLogin(userDetails.getName());
    }

    @Test
    void testUserDoesNotExist() {
        Set<String> authorities = new HashSet<>();
        authorities.add(Authority.CUSTOMER_ROLE);
        AuthenticatedUserDetails userDetails = new AuthenticatedUserDetails("12345", "John", authorities);

        Exception e = new IllegalArgumentException("Customer with id '" + userDetails.getName() + " was not found.");
        when(dbService.getCustomerByLogin(userDetails.getName())).thenThrow(e);

        Exception eResult = assertThrows(IllegalArgumentException.class, () -> customerManager.me(userDetails));
        assertEquals(e.getMessage(), eResult.getMessage());

        verify(dbService, times(1)).getCustomerByLogin(userDetails.getName());
    }


    /*
     *
     *   Tests for 'delete' method
     *
     */

    @Test
    void testDeleteCustomer() {

        doNothing().when(dbService).deleteCustomer(customerInput.id);

        customerManager.deleteCustomer(customerInput.id);

        verify(dbService, times(1)).deleteCustomer(customerInput.id);
    }

    @Test
    void testDeleteCustomerRuntimeException() {

        Exception e = new RuntimeException(new SQLException("Reason"));
        doThrow(e).when(dbService).deleteCustomer(customerInput.id);

        Exception eResult = assertThrows(RuntimeException.class, () -> customerManager.deleteCustomer(customerInput.id));

        assertEquals(e.getMessage(), eResult.getMessage());

        verify(dbService, times(1)).deleteCustomer(customerInput.id);
    }




    /*
     *
     *   Tests for 'topUpBalance' method
     *
     */

    private void checkTopUpBalanceWithCorrectData(int money) {
        TopUpBalancePojo balancePojo = new TopUpBalancePojo();
        balancePojo.customerId = customerInput.id;
        balancePojo.money = money;
        int startBalance = customerInput.balance;

        when(dbService.getCustomer(customerInput.id)).thenReturn(customerInput);

        CustomerPojo changedCustomer = customerManager.topUpBalance(balancePojo);

        assertEquals(startBalance + balancePojo.money, changedCustomer.balance);

        verify(dbService, times(1)).getCustomer(customerInput.id);
        verify(dbService, times(1)).editCustomer(changedCustomer);
    }

    private void checkTopUpBalanceWithIncorrectData(int money) {
        TopUpBalancePojo balancePojo = new TopUpBalancePojo();
        balancePojo.customerId = customerInput.id;
        balancePojo.money = money;

        when(dbService.getCustomer(customerInput.id)).thenReturn(customerInput);

        Exception e = assertThrows(IllegalArgumentException.class, () -> customerManager.topUpBalance(balancePojo));
        assertEquals(e.getMessage(), TOP_UP_BALANCE_NOT_GREATER_ZERO.getMessage());

        verify(dbService, times(0)).getCustomer(customerInput.id);
    }

    @Test
    void testTopUpBalance() {
        checkTopUpBalanceWithCorrectData(100);
    }

    @Test
    void testTopUpBalanceHuge() {
        checkTopUpBalanceWithCorrectData(123456789);
    }

    @Test
    void testTopUpBalanceMinimum() {
        checkTopUpBalanceWithCorrectData(1);
    }

    @Test
    void testTopUpBalanceZero() {
        checkTopUpBalanceWithIncorrectData(0);
    }

    @Test
    void testTopUpBalanceNegative() {
        checkTopUpBalanceWithIncorrectData(-100);
    }

    @Test
    void testTopUpBalanceNegativeMin() {
        checkTopUpBalanceWithIncorrectData(-1);
    }

    @Test
    void testTopUpBalanceNegativeHuge() {
        checkTopUpBalanceWithIncorrectData(-123456789);
    }
}
