package org.nsu.fit.tm_backend.operations;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.nsu.fit.tm_backend.database.IDBService;
import org.nsu.fit.tm_backend.database.data.CustomerPojo;
import org.nsu.fit.tm_backend.database.data.PlanPojo;
import org.nsu.fit.tm_backend.database.data.SubscriptionPojo;
import org.nsu.fit.tm_backend.manager.CustomerManager;
import org.nsu.fit.tm_backend.manager.SubscriptionManager;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class StatisticOperationTest {
    // Лабораторная 2: покрыть юнит тестами класс StatisticOperation на 100%.
    private Logger logger;
    private IDBService dbService;
    private CustomerManager customerManager;
    private SubscriptionManager subscriptionManager;


    @BeforeEach
    void init() {
        // Создаем mock объекты.
        dbService = mock(IDBService.class);
        logger = mock(Logger.class);

        // Создаем класс, методы которого будем тестировать,
        // и передаем ему наши mock объекты.
        customerManager = new CustomerManager(dbService, logger);

        subscriptionManager = new SubscriptionManager(dbService, logger);
    }


    /*
     *
     *   Tests for StatisticOperation constructor
     *
     */

    @Test
    void testStatisticOperationIllegalCustomerManager() {
        Exception e = assertThrows(IllegalArgumentException.class, () -> new StatisticOperation(null, subscriptionManager, new ArrayList<>()));
        assertEquals("customerManager", e.getMessage());
    }

    @Test
    void testStatisticOperationIllegalSubscriptionManager() {
        Exception e = assertThrows(IllegalArgumentException.class, () -> new StatisticOperation(customerManager, null, new ArrayList<>()));
        assertEquals("subscriptionManager", e.getMessage());
    }

    @Test
    void testStatisticOperationIllegalCustomersIds() {
        Exception e = assertThrows(IllegalArgumentException.class, () -> new StatisticOperation(customerManager, subscriptionManager, null));
        assertEquals("customerIds", e.getMessage());
    }


    /*
     *
     *   Tests for 'Execute' method
     *
     */

    private void prepareCustomers(List<CustomerPojo> customers, List<UUID> customerIds, int count) {
        for (int i = 0; i < count; ++i) {
            CustomerPojo customer = new CustomerPojo();
            customer.id = UUID.randomUUID();
            customer.firstName = "John" + count;
            customer.lastName = "Wick" + count;
            customer.login = "n"  + count + "@mail.com";
            customer.pass = "Baba_Jaga" + count;
            customer.balance = 1;

            customers.add(customer);
            customerIds.add(customer.id);

            when(dbService.getCustomer(customer.id)).thenReturn(customer);
        }
    }

    private void prepareSubscriptionForOneCustomer(List<SubscriptionPojo> subscriptions, CustomerPojo customer, int count) {
        List<SubscriptionPojo> localSubscriptions = new ArrayList<>();
        for (int i = 0; i < count; ++i) {
            SubscriptionPojo subscription = new SubscriptionPojo();
            subscription.id = UUID.randomUUID();
            subscription.planId = UUID.randomUUID();
            subscription.customerId = customer.id;
            subscription.planName = "Plan";
            subscription.planDetails = "Details of this amazing plan";
            subscription.planFee = 1;

            localSubscriptions.add(subscription);
            subscriptions.add(subscription);
        }
        when(dbService.getSubscriptions(customer.id)).thenReturn(localSubscriptions);
    }

    private void preparePlansAndSubscriptions(List<SubscriptionPojo> subscriptions, List<CustomerPojo> customers, int countForEach) {
        for (CustomerPojo customer : customers) {
            prepareSubscriptionForOneCustomer(subscriptions, customer, countForEach);
        }
        when(dbService.getSubscriptions()).thenReturn(subscriptions);

        List<PlanPojo> plans = new ArrayList<>();
        for (SubscriptionPojo subscription : subscriptions) {
            PlanPojo plan = new PlanPojo();
            plan.id = subscription.planId;
            plan.name = subscription.planName;
            plan.details = subscription.planDetails;
            plan.fee = subscription.planFee;
            plans.add(plan);
        }
        when(dbService.getPlans()).thenReturn(plans);
    }

    private void checkExecuteSomeCustomersSomeSubscriptions(int customerCount, int subscriptionCount) {
        List<CustomerPojo> customers = new ArrayList<>();
        List<UUID> customerIds = new ArrayList<>();
        List<SubscriptionPojo> subscriptions = new ArrayList<>();

        prepareCustomers(customers, customerIds, customerCount);
        preparePlansAndSubscriptions(subscriptions, customers, subscriptionCount);

        StatisticOperation statisticOperation = new StatisticOperation(customerManager, subscriptionManager, customerIds);

        StatisticOperation.StatisticOperationResult result = statisticOperation.Execute();

        assertEquals(customerCount, result.overallBalance);
        assertEquals(customerCount*subscriptionCount, result.overallFee);

        for(UUID customerId : customerIds) {
            verify(dbService, times(1)).getCustomer(customerId);
            verify(dbService, times(1)).getSubscriptions(customerId);
        }

        verify(dbService, times(customerCount)).getCustomer(any());
        verify(dbService, times(customerCount)).getSubscriptions(any());
    }

    @Test
    void testExecuteNoCustomers() {
        List<UUID> customerIds = new ArrayList<>();

        StatisticOperation statisticOperation = new StatisticOperation(customerManager, subscriptionManager, customerIds);

        StatisticOperation.StatisticOperationResult result = statisticOperation.Execute();

        assertEquals(0, result.overallBalance);
        assertEquals(0, result.overallFee);

        verify(dbService, times(0)).getCustomer(any());
        verify(dbService, times(0)).getSubscriptions(any());
        verify(dbService, times(0)).getSubscriptions();
        verify(dbService, times(0)).getCustomers();
    }

    @Test
    void testExecuteOneCustomerNoSubscriptions() {
        List<CustomerPojo> customers = new ArrayList<>();
        List<UUID> customerIds = new ArrayList<>();
        List<SubscriptionPojo> subscriptions = new ArrayList<>();

        prepareCustomers(customers, customerIds, 1);
        when(dbService.getSubscriptions(customerIds.get(0))).thenReturn(subscriptions);

        StatisticOperation statisticOperation = new StatisticOperation(customerManager, subscriptionManager, customerIds);

        StatisticOperation.StatisticOperationResult result = statisticOperation.Execute();

        assertEquals(1, result.overallBalance);
        assertEquals(0, result.overallFee);

        verify(dbService, times(1)).getCustomer(customerIds.get(0));
        verify(dbService, times(1)).getSubscriptions(customerIds.get(0));
    }

    @Test
    void testExecuteOneCustomerOneSubscription() {
        checkExecuteSomeCustomersSomeSubscriptions(1, 1);
    }

    @Test
    void testExecuteTwoSubscriptions() {
        checkExecuteSomeCustomersSomeSubscriptions(1, 2);
    }

    @Test
    void testExecuteFiveSubscriptions() {
        checkExecuteSomeCustomersSomeSubscriptions(1, 5);
    }

    @Test
    void testExecuteTwoCustomers() {
        checkExecuteSomeCustomersSomeSubscriptions(2, 1);
    }

    @Test
    void testExecuteFiveCustomers() {
        checkExecuteSomeCustomersSomeSubscriptions(5, 1);
    }

    @Test
    void testExecuteTwoCustomersTwoSubscriptions() {
        checkExecuteSomeCustomersSomeSubscriptions(2, 2);
    }

    @Test
    void testExecuteTwoCustomersFiveSubscriptions() {
        checkExecuteSomeCustomersSomeSubscriptions(2, 5);
    }

    @Test
    void testExecuteFiveCustomersTwoSubscriptions() {
        checkExecuteSomeCustomersSomeSubscriptions(5, 2);
    }

    @Test
    void testExecuteFiveCustomersFiveSubscriptions() {
        checkExecuteSomeCustomersSomeSubscriptions(5, 5);
    }
}
