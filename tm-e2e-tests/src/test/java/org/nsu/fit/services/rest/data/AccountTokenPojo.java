package org.nsu.fit.services.rest.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;
import java.util.UUID;

@EqualsAndHashCode
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountTokenPojo {
    @JsonProperty("id")
    public UUID id;

    @JsonProperty("authorities")
    public Set<String> authorities;

    @JsonProperty("token")
    public String token;
}
