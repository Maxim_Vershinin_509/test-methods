package org.nsu.fit.shared;

import org.nsu.fit.services.browser.Browser;
import org.openqa.selenium.support.PageFactory;

public class Screen {
    protected Browser browser;

    public Screen(Browser browser) {
        PageFactory.initElements(browser.getWebDriver(), this);
        this.browser = browser;
    }
}
