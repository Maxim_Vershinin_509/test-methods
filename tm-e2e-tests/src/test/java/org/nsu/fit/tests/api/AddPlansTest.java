package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class AddPlansTest {
    private AccountTokenPojo adminToken;
    private final RestClient restClient = new RestClient();

    @BeforeTest
    public void adminAuth() {
        adminToken = new RestClient().authenticate("admin", "setup");
    }

    @Test(description = "Add random plan.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Add plan feature.")
    public void addRandomPlanActionTest() {
        List<PlanPojo> allPlanesBeforeAdd = restClient.getPlans(adminToken, null);
        PlanPojo addedPlan = restClient.createRandomPlan(adminToken);
        List<PlanPojo> allPlanesAfterAdd = restClient.getPlans(adminToken, null);

        allPlanesBeforeAdd.add(addedPlan);

        Assert.assertEquals(allPlanesBeforeAdd, allPlanesAfterAdd);
    }

}
