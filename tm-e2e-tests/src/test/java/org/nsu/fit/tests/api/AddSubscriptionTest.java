package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.nsu.fit.services.rest.data.SubscriptionPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;

public class AddSubscriptionTest {
    private AccountTokenPojo adminToken;
    private final RestClient restClient = new RestClient();

    @BeforeTest
    public void adminAuth() {
        adminToken = new RestClient().authenticate("admin", "setup");
    }

    @Test(description = "Add subscription with random UUID.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Add subscription feature.")
    public void addSubscriptionWithRandomPlanActionTest() {
        CustomerPojo customerPojo = restClient.createAutoGeneratedCustomer(adminToken);
        AccountTokenPojo customerToken = restClient.authenticate(customerPojo.login, customerPojo.pass);
        UUID randomUUID = UUID.randomUUID();
        SubscriptionPojo subscriptionPojo = restClient.createSubscription(customerToken, randomUUID);

        Assert.assertEquals(customerPojo.id, subscriptionPojo.customerId);
        Assert.assertEquals(randomUUID, subscriptionPojo.planId);
    }

    @Test(description = "Add subscription with exists UUID.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Add subscription feature.")
    public void addSubscriptionWithExistsPlanActionTest() {
        CustomerPojo customerPojo = restClient.createAutoGeneratedCustomer(adminToken);
        PlanPojo planPojo = restClient.createRandomPlan(adminToken);
        AccountTokenPojo customerToken = restClient.authenticate(customerPojo.login, customerPojo.pass);
        SubscriptionPojo subscriptionPojo = restClient.createSubscription(customerToken, planPojo.id);

        Assert.assertEquals(customerPojo.id, subscriptionPojo.customerId);
        Assert.assertEquals(planPojo.id, subscriptionPojo.planId);
    }
}
