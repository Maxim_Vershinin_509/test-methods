package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.ws.rs.WebApplicationException;
import java.util.List;
import java.util.UUID;

public class DeleteCustomerTest {
    private RestClient restClient;
    private AccountTokenPojo adminToken;
    private CustomerPojo userPojo;

    @BeforeClass(description = "Authenticate as admin.")
    public void authAsAdmin() {
        restClient = new RestClient();
        adminToken = restClient.authenticate("admin", "setup");
        userPojo = new CustomerPojo();
        userPojo.firstName = "John";
        userPojo.lastName = "Wick";
        userPojo.login = "login_for_delete_test@example.com";
        userPojo.pass = "strongpass";

    }

    @Test(description = "Delete customer who does not exist.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Delete customer feature.")
    public void deleteCustomerWhoDoesntExistTest() {
        restClient.deleteCustomer(UUID.randomUUID().toString(), adminToken);
    }

    @Test(description = "Delete existing customer.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Delete customer feature.")
    public void deleteExistingCustomerTest() {
        CustomerPojo customerPojo = restClient.createCustomer(userPojo, adminToken);
        restClient.deleteCustomer(customerPojo.id.toString(), adminToken);
        List<CustomerPojo> customers = new RestClient().getCustomers(userPojo.login, adminToken);
        Assert.assertEquals(customers.size(), 0);
    }
}
