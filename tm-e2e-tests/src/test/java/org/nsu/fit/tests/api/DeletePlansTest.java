package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.ws.rs.WebApplicationException;
import java.util.*;

public class DeletePlansTest {
    private AccountTokenPojo adminToken;

    @BeforeTest
    public void adminAuth() {
        adminToken = new RestClient().authenticate("admin", "setup");
    }

    @Test(description = "Delete plan of null id")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Delete plans feature.")
    public void deleteNullPlanActionTest() {
        Assert.assertThrows(WebApplicationException.class, () -> new RestClient().detetePlan(adminToken, null));
    }

    @Test(description = "Delete plan of not exists id")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Delete plans feature.")
    public void deleteNotExistsPlanActionTest() {
        Assert.assertNull(new RestClient().detetePlan(adminToken, UUID.randomUUID()));
    }

    @Test(description = "Delete plan of exists id")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Delete plans feature.")
    public void deleteExistsPlanActionTest() {
        PlanPojo planPojo = new RestClient().createRandomPlan(adminToken);
        List<PlanPojo> allPlanesBeforeDelete = new RestClient().getPlans(adminToken, null);
        new RestClient().detetePlan(adminToken, planPojo.id);
        List<PlanPojo> allPlanesAfterDelete = new RestClient().getPlans(adminToken, null);

        List<PlanPojo> allPlanesAfterDeleteWithOldPlan = new ArrayList<>(allPlanesAfterDelete);
        allPlanesAfterDeleteWithOldPlan.add(planPojo);
        Assert.assertEquals(allPlanesAfterDeleteWithOldPlan, allPlanesBeforeDelete);

        Assert.assertNull(new RestClient().detetePlan(adminToken, planPojo.id));
    }

}
