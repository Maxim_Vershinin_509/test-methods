package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.nsu.fit.services.rest.data.PlanPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import javax.ws.rs.WebApplicationException;
import java.util.ArrayList;
import java.util.List;

public class GetAvailablePlansTest {
    private AccountTokenPojo adminToken;
    private RestClient restClient;
    private List<PlanPojo> randomPlans;

    @BeforeTest
    public void adminAuth() {
        restClient = new RestClient();
        adminToken = restClient.authenticate("admin", "setup");
        randomPlans = new ArrayList<>();
        randomPlans.add(restClient.createRandomPlan(adminToken));
        randomPlans.add(restClient.createRandomPlan(adminToken));
    }

    @Test(description = "Get plans of new customer")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Get available plans feature.")
    public void getPlansForAdminTest() {
        Assert.expectThrows(WebApplicationException.class, () -> restClient.getAvailablePlans(adminToken));
    }

    @Test(description = "Get plans of new customer")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Get available plans feature.")
    public void getPlansOfNewCustomerTest() {
        CustomerPojo customerPojo = restClient.createAutoGeneratedCustomer(adminToken);
        List<PlanPojo> allPlans = restClient.getPlans(adminToken, null);
        AccountTokenPojo userToken = restClient.authenticate(customerPojo.login, customerPojo.pass);
        List<PlanPojo> availablePlans = restClient.getAvailablePlans(userToken);
        Assert.assertEquals(availablePlans.size(), allPlans.size());
    }

    @Test(description = "Get available plans of customer with plans")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Get available plans feature.")
    public void getPlansOfCustomerWithPlansTest() {
        CustomerPojo customerPojo = restClient.createAutoGeneratedCustomer(adminToken);
        List<PlanPojo> allPlans = restClient.getPlans(adminToken, null);
        AccountTokenPojo userToken = restClient.authenticate(customerPojo.login, customerPojo.pass);
        restClient.createSubscription(userToken, randomPlans.get(0).id);
        restClient.createSubscription(userToken, randomPlans.get(1).id);
        List<PlanPojo> availablePlans = restClient.getAvailablePlans(userToken);
        Assert.assertEquals(availablePlans.size() + 2, allPlans.size());
        availablePlans.addAll(randomPlans);
        for (PlanPojo plan : availablePlans) {
            Assert.assertTrue(allPlans.contains(plan));
        }
    }
}
