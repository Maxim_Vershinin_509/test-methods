package org.nsu.fit.tests.api;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.services.rest.RestClient;
import org.nsu.fit.services.rest.data.AccountTokenPojo;
import org.nsu.fit.services.rest.data.CustomerPojo;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class GetCustomersTest {
    private AccountTokenPojo adminToken;
    private String randomUserLogin;

    @BeforeTest
    public void adminAuth() {
        adminToken = new RestClient().authenticate("admin", "setup");
        new RestClient().createAutoGeneratedCustomer(adminToken);
    }

    @Test(description = "Get all customers")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Get customers feature.")
    public void getAllCustomersTest() {
        List<CustomerPojo> customers = new RestClient().getCustomers(null, adminToken);
        Assert.assertTrue(customers.size() > 0);
        randomUserLogin = customers.get(0).login;
    }

    @Test(description = "Get customer by login", dependsOnMethods = "getAllCustomersTest")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Get customers feature.")
    public void getCustomerTest() {
        List<CustomerPojo> customers = new RestClient().getCustomers(randomUserLogin, adminToken);
        Assert.assertEquals(customers.size(), 1);
        Assert.assertEquals(customers.get(0).login, randomUserLogin);
    }
}
