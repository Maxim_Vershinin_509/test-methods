package org.nsu.fit.tests.ui;

import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.nsu.fit.tests.ui.data.Plan;
import org.nsu.fit.tests.ui.screen.CustomerScreen;
import org.nsu.fit.tests.ui.screen.LoginScreen;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CreateSubscriptionTest extends BaseTest {
    @Test(description = "Create subscription via UI.")
    @Severity(SeverityLevel.BLOCKER)
    @Feature("Subscription feature.")
    public void createSubscriptionTest() {

        CustomerScreen screen = new LoginScreen(browser)
                .loginAsCustomer("john@example.com", "strongpass");

        Plan planToBuy = screen.findPlan("test");

        Assert.assertNotNull(planToBuy);

        Plan subscription = screen
                .buyFirstPlan()
                .buyFirstPlanOk()
                .findSubscription(planToBuy.name);

        Assert.assertEquals(subscription.name, planToBuy.name);
        Assert.assertEquals(subscription.details, planToBuy.details);
        Assert.assertEquals(subscription.fee, planToBuy.fee);
    }
}
